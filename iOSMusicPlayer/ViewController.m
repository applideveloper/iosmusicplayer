//
//  ViewController.m
//  iOSMusicPlayer
//
//  Created by 荒木 敦 on 2014/10/17.
//  Copyright (c) 2014年 WishMatch. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController


- (BOOL)canBecomeFirstResponder{
    return YES;
}

- (void)viewDidAppear:(BOOL)animated{
    [self becomeFirstResponder];
}

- (void)viewDidDisappear:(BOOL)animated{
    [self resignFirstResponder];
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event{
    MPMusicPlayerController *iPodMusicPlayer = [MPMusicPlayerController iPodMusicPlayer];
    MPMediaItem *beforeItem = iPodMusicPlayer.nowPlayingItem;
    
    NSNumber *beforeID = [beforeItem valueForProperty:MPMediaItemPropertyPersistentID];
    //前に再生していた曲と同じなら再度選択する
    int i = 0;
    while(1){
        [iPodMusicPlayer stop];
        [iPodMusicPlayer play];
        MPMediaItem *currentItem = iPodMusicPlayer.nowPlayingItem;
        NSNumber *currentID = [currentItem valueForProperty:MPMediaItemPropertyPersistentID];
        if(![currentID isEqual:beforeID]){
            break;
        }
        i++;
        if(i > 3)break;//4回同じならそれを再生する
    }
}

- (IBAction)pick:(id)sender {
    //MPMediaPickerControllerのインスタンスを作成
    MPMediaPickerController *mediaPickerController = [[MPMediaPickerController alloc]init];
    //デリゲートを設定
    mediaPickerController.delegate = self;
    //複数の曲を選択可能にする
    mediaPickerController.allowsPickingMultipleItems = YES;
    
    mediaPickerController.showsCloudItems = NO;
    
    //ピッカーを表示する
    [self stopTimer];
    [self presentModalViewController:mediaPickerController animated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self becomeFirstResponder];
    
    MPMusicPlayerController *iPodMusicPlayer = [MPMusicPlayerController iPodMusicPlayer];
    iPodMusicPlayer.repeatMode = MPMusicRepeatModeAll;
    //    iPodMusicPlayer.shuffleMode = MPMusicShuffleModeSongs;
    iPodMusicPlayer.shuffleMode = MPMusicShuffleModeOff;
    
    MPMediaItem *item = iPodMusicPlayer.nowPlayingItem;
    //現在再生中の曲のアートワークを取得する
    MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
    //artwork.bounds.sizeでサイズが取得できる。
    UIImage *image = [artwork imageWithSize:artwork.imageCropRect.size];
    
    NSLog(@"%@", NSStringFromCGRect(artwork.imageCropRect));
    NSLog(@"%@", NSStringFromCGRect(artwork.bounds));
    
    //UIImageViewに表示する
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:self.view.bounds];
    imageView.tag = 1;
    [self.view addSubview:imageView];
    [self.view sendSubviewToBack:imageView];
    
    //縦横比に基づいて表示するようにする
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = image;
    
    //通知センターに登録する
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(nowPlayingItemDidChangeNotification:)
                   name:MPMusicPlayerControllerNowPlayingItemDidChangeNotification
                 object:nil];
    
    [iPodMusicPlayer beginGeneratingPlaybackNotifications];
    
    //nowPlayingItemがあれば情報を更新する
    if(iPodMusicPlayer.nowPlayingItem){
        [self updateInformation:iPodMusicPlayer.nowPlayingItem];
        //再生位置も更新されるようにする
        [self startTimer];
    }
}

//通知を受けるメソッド
-(void)nowPlayingItemDidChangeNotification:(NSNotification*)notification{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    
    MPMusicPlayerController *iPodMusicPlayer = [MPMusicPlayerController iPodMusicPlayer];
    
    MPMediaItem *item = iPodMusicPlayer.nowPlayingItem;
    
    //MPMusicPlayerControllerからnowPlayingItemを取得し、情報を更新する
    //    MPMediaItem *item = [MPMusicPlayerController iPodMusicPlayer].nowPlayingItem;
    
    //    MPMediaItem *item = iPodMusicPlayer.nowPlayingItem;
    //現在再生中の曲のアートワークを取得する
    MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
    //artwork.bounds.sizeでサイズが取得できる。
    UIImage *image = [artwork imageWithSize:artwork.imageCropRect.size];
    
    NSLog(@"%@", NSStringFromCGRect(artwork.imageCropRect));
    NSLog(@"%@", NSStringFromCGRect(artwork.bounds));
    
    [[self.view viewWithTag:1] removeFromSuperview];
    
    //UIImageViewに表示する
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:self.view.bounds];
    
    //    [self.view insertSubview:imageView atIndex:1 ];
    imageView.tag = 1;
    [self.view addSubview:imageView];
    [self.view sendSubviewToBack:imageView];
    
    //    [self.view exchangeSubviewAtIndex:1 withSubviewAtIndex:2];
    
    //縦横比に基づいて表示するようにする
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = image;
    
    if(item)[self updateInformation:item];
}

-(void)updateInformation:(MPMediaItem*)item{
    titleLabel.text = [item valueForProperty:MPMediaItemPropertyTitle];
    artistLabel.text = [item valueForProperty:MPMediaItemPropertyAlbumTitle];
    albumLabel.text = [item valueForProperty:MPMediaItemPropertyArtist];
    
    NSNumber *playbackDuration = [item valueForProperty:MPMediaItemPropertyPlaybackDuration];
    currentTimeSlider.maximumValue = [playbackDuration doubleValue];
}

//曲が選択された場合に呼ばれる
- (void)mediaPicker:(MPMediaPickerController *)mediaPicker
  didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection{
    //MPMusicPlayerControllerのインスタンスを取得(iPodMusicPlayer)
    MPMusicPlayerController *iPodMusicPlayer = [MPMusicPlayerController iPodMusicPlayer];
    //ピックしたMPMediaItemCollectionをセットする
    [iPodMusicPlayer setQueueWithItemCollection:mediaItemCollection];
    
    //再生する
    [iPodMusicPlayer play];
    
    //MPMediaPickerControllerを閉じ、破棄する
    [mediaPicker dismissModalViewControllerAnimated:YES];
    [mediaPicker release];
    
    [self startTimer];
}

//選択がキャンセルされた場合に呼ばれる
- (void)mediaPickerDidCancel:(MPMediaPickerController *)mediaPicker{
    //MPMediaPickerControllerを閉じ、破棄する
    [mediaPicker dismissModalViewControllerAnimated:YES];
    [mediaPicker release];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sliderValueChanged:(UISlider*)sender {
    [self stopTimer];
    [MPMusicPlayerController iPodMusicPlayer].currentPlaybackTime = sender.value;
    [self startTimer];
}

- (IBAction)stop:(id)sender {
    [[MPMusicPlayerController iPodMusicPlayer] stop];
}

- (IBAction)play:(id)sender {
    [[MPMusicPlayerController iPodMusicPlayer] play];
}

- (IBAction)pause:(id)sender {
    [[MPMusicPlayerController iPodMusicPlayer] pause];
}

- (IBAction)beginSeekingForward:(id)sender {
    NSLog(@"beginSeekingForward");
    [[MPMusicPlayerController iPodMusicPlayer] beginSeekingForward];
}

- (IBAction)beginSeekingBackward:(id)sender {
    NSLog(@"beginSeekingBackward");
    [[MPMusicPlayerController iPodMusicPlayer] beginSeekingBackward];
}

- (IBAction)endSeeking:(id)sender {
    [[MPMusicPlayerController iPodMusicPlayer] endSeeking];
}

-(void)updateSlider{
    currentTimeSlider.value = [MPMusicPlayerController iPodMusicPlayer].currentPlaybackTime;
}

-(void)startTimer{
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                             target:self
                                           selector:@selector(updateSlider)
                                           userInfo:nil
                                            repeats:YES];
    [timer retain];
}

-(void)stopTimer{
    [timer invalidate];
    timer = nil;
}

- (IBAction)next:(id)sender {
    [[MPMusicPlayerController iPodMusicPlayer] skipToNextItem];
}

- (IBAction)begin:(id)sender {
    [[MPMusicPlayerController iPodMusicPlayer] skipToBeginning];
}

- (IBAction)prev:(id)sender {
    [[MPMusicPlayerController iPodMusicPlayer] skipToPreviousItem];
}


- (void)dealloc {
    if(timer)[self stopTimer];
    [currentTimeSlider release];
    [currentTimeSlider release];
    [super dealloc];
}

@end
