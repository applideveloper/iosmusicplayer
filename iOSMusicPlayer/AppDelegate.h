//
//  AppDelegate.h
//  iOSMusicPlayer
//
//  Created by 荒木 敦 on 2014/10/17.
//  Copyright (c) 2014年 WishMatch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

