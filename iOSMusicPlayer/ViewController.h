//
//  ViewController.h
//  iOSMusicPlayer
//
//  Created by 荒木 敦 on 2014/10/17.
//  Copyright (c) 2014年 WishMatch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface ViewController : UIViewController <MPMediaPickerControllerDelegate> {
    
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UILabel *artistLabel;
    __weak IBOutlet UILabel *albumLabel;

    IBOutlet UISlider *currentTimeSlider;
    
    NSTimer *timer;
}

- (IBAction)pick:(id)sender;

- (IBAction)play:(id)sender;
- (IBAction)stop:(id)sender;
- (IBAction)pause:(id)sender;

- (IBAction)sliderValueChanged:(UISlider*)sender;


- (IBAction)next:(id)sender;
- (IBAction)begin:(id)sender;
- (IBAction)prev:(id)sender;

-(void)startTimer;
-(void)stopTimer;

-(void)updateInformation:(MPMediaItem*)item;

- (IBAction)beginSeekingForward:(id)sender;
- (IBAction)beginSeekingBackward:(id)sender;

- (IBAction)endSeeking:(id)sender;

@end

